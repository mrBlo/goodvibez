
package com.blo.goodvibez;

import com.google.firebase.database.ServerValue;

import java.util.Calendar;
import java.util.HashMap;

public class FriendlyMessage {

    private String userID;
    private String text;
    private String name;
    private String photoUrl;
    private long messageTime;
    Calendar cal = Calendar.getInstance();


    public FriendlyMessage() {
    }

    public FriendlyMessage(String text, String name, String photoUrl) {
        this.text = text;
        this.name = name;
        this.photoUrl = photoUrl;
    }

    public FriendlyMessage(String userID, String text, String name, String photoUrl) {
        this.userID = userID;
        this.text = text;
        this.name = name;
        this.photoUrl = photoUrl;

        //  this.messageTime = messageTime;
    }
//    public FriendlyMessage(String text, String name, String photoUrl,long messageTime) {
//        this.text = text;
//        this.name = name;
//        this.photoUrl = photoUrl;
//        this.messageTime = messageTime;
//    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public long getMessageTime() {
        return messageTime;
    }

//    public HashMap<String, Object> getTimestampCreated(){
//        return timestampCreated;
//    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }
}
