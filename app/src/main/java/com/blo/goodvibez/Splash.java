package com.blo.goodvibez;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Splash extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();
        Thread timer = new Thread() {
            public void run() {
                try {
                    // Toast.makeText(MainActivity.this,"Welcome..",Toast.LENGTH_SHORT).show();
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent in = new Intent(Splash.this, Home.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                    finish();
                }

            }


        };
        timer.start();


    }


}
